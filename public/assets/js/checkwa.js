function onLoadCartNumbers(){
    let productNumbers = localStorage.getItem('cartNumbers');
    if (productNumbers){
       document.querySelector('.cart span').textContent = productNumbers; 
    }
}
function totalCost (product){
    console.log("the product price is" , product.price);
    let cartCost = localStorage.getItem('totalCost');

    if(cartCost != null){
        cartCost = parseInt(cartCost);
        localStorage.setItem("totalCost", cartCost + product.price);
    } else {
        localStorage.setItem("totalCost", product.price);
    }
}
function displayCart(){
    let cartItems = localStorage.getItem("productsInCart");
    cartItems = JSON.parse(cartItems);
    let productContainer = document.querySelector(".summary");
    let cartCost = localStorage.getItem('totalCost');
    const shipping = 100.00;
    if(cartItems && productContainer){
        productContainer.innerHTML = '';
            productContainer.innerHTML += `
            <div class="product">
            <p class="sum">Order Summary:</p>
            <p id="sub">Subtotal:${cartCost}</p>
            <p>Shipping Fee:${shipping}</p>
            <p>Shipping Fee Discount:</p>
            <label for="">
              <input type="text" name="" id="" class="form-vouch" placeholder="Voucher Code">
            </label>
            <div class="voc">
              <label for="">
                <input type="submit" class="btn-vouch" value="Enter Voucher">
              </label>
            </div>
    
            <div class="btn-orders">
              <input type="button" class="btn-order" value="Place Order">
            </div>
            <div class="tot">
              <h1>Total in Cart:${parseInt(cartCost) + parseInt(shipping)}</h1>
            </div>
            </div>
            `;
    }


}

function succesfull() {
  alert("THANK YOU FOR PURCHASING");
}


displayCart();